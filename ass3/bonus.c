#include <stdio.h>
#include <stdlib.h>
#define BOARD_SIZE 4

//檢查勝利
int checkWinner(char* name[10], int who, int counter, int board_size, char* board[board_size][board_size]){
	char* winner = NULL;
	if((board[1][1] == board[1][2] & board[1][1] == board[1][3] & board[1][3] != winner) |
		(board[2][1] == board[2][2] & board[2][1] == board[2][3] & board[2][3] != winner) |
		(board[3][1] == board[3][2] & board[3][1] == board[3][3] & board[3][3] != winner) |
		(board[1][1] == board[2][1] & board[2][1] == board[3][1] & board[3][1] != winner) |
 		(board[1][2] == board[2][2] & board[2][2] == board[3][2] & board[3][2] != winner) |
		(board[1][3] == board[2][3] & board[2][3] == board[3][3] & board[3][3] != winner) |
 		(board[1][1] == board[2][2] & board[2][2] == board[3][3] & board[3][3] != winner) |
		(board[1][3] == board[2][2] & board[2][2] == board[3][1] & board[3][1] != winner)
	){	
		if(who == 1){
			printf("%s WIN\n", name);
		}else{
			printf("YOU LOSE\n");
			printf("WINNER IS %s\n", name);	
			}
		counter = 9;
	}else{
		counter++;
	}
	return counter;
}
//印出棋盤 
void printBoard(int board_size, char* board[board_size][board_size]){
	int i,j;
	for(i = 0; i < board_size; i++){
		for(j = 0; j < board_size; j++){
			if((i == 0 | j == 0)){
				board[i][j] = " * ";
			}  
			if(board[i][j]){
				printf(board[i][j]);
			} 	
			else{
				printf("   ");
			} 	
			if(j == board_size-1){
				printf("\n");	
			} 
		}
	}
	printf("=============================\n");
}

//玩家下 
void playerMove(int board_size, char* board[board_size][board_size]){
	int s,t;
	printf("Input (row, column) to place chess: \n");
	scanf("%d,%d", &s, &t);
	if(board[s][t] != NULL | s >= board_size | t >= board_size){
		printf("wrong movement\n");
		playerMove(board_size ,board);
	}else{
		board[s][t] = " O ";
		printBoard(board_size ,board);
	}
}


//笨電腦下，無求勝意志 
void computerMove(int board_size, char* board[board_size][board_size]){
//	srand( (unsigned)time(NULL) );
	srand(time(0));
	int s= rand()%3 + 1;
	int t= rand()%3 + 1;
	if(board[s][t] != NULL | s >= board_size | t >= board_size){
		printf("computer is shit\n");
		computerMove(board_size ,board);
	} 	
	else{
		board[s][t] = " X ";
		printBoard(board_size ,board);
	} 
}

void main(){
	char* x[10] = { NULL };
	char* y[10] = { NULL };
	char* board[BOARD_SIZE][BOARD_SIZE] = { NULL };
	int m = 1;

	printf("Input your name: \n");
	scanf("%s",&x);
	printf("Input opponent's name: \n");
	scanf("%s",&y);
	printf("%s V.S. %s\n", x, y);
	
    while(m < 9){
		if(m%2 == 1){
			computerMove(BOARD_SIZE, board);
			m = checkWinner(y, 0, m, BOARD_SIZE, board);
		}else{
			playerMove(BOARD_SIZE ,board);
			m = checkWinner(x, 1,m , BOARD_SIZE, board);
		}		
	}
	

}

